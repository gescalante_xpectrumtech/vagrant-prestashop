#!/usr/bin/env bash

PRESTASHOP_FILE_V=prestashop_1.6.0.8.zip
DATABASE_NAME=prestashop
PRESTASHOP_DIR_NAME=prestashop

debconf-set-selections <<< 'mysql-server mysql-server/root_password password qpalwosk10'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password qpalwosk10'

sudo apt-get update && sudo apt-get upgrade -y
sudo apt-get install -y unzip

#Apache
sudo apt-get install -y apache2 libapache2-mod-php5

#MySql
sudo apt-get install -y mysql-server

#MySql driver
sudo apt-get install -y mysql-server php5-mysql

#MCrypt
sudo apt-get install -y php5 php5-mcrypt

#GD Library
sudo apt-get install -y php5-gd

#SendMail
sudo apt-get install -y sendmail

#Prestashop
sudo rm -f -R "/vagrant/prestashop"
cd /vagrant
wget http://www.prestashop.com/download/old/$PRESTASHOP_FILE_V
unzip -o $PRESTASHOP_FILE_V
sudo rm ./$PRESTASHOP_FILE_V

sudo mv /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf.old
sudo echo "<VirtualHost *:80>
		DocumentRoot '/vagrant/prestashop'
  		<Directory '/vagrant/prestashop'>
    			AllowOverride all
	  	</Directory>
	   </VirtualHost>" > /etc/apache2/sites-available/000-default.conf

#Changing owner
sudo chown -R www-data /vagrant/prestashop/*
sudo chgrp -R www-data /vagrant/prestashop/*

#Enabling mcrypt
sudo echo "extension=mcrypt.so" >> /etc/php5/apache2/php.ini

#Allowing access to vagrant dir
sudo echo "<Directory /vagrant/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
	</Directory>" >> /etc/apache2/apache2.conf

#Creating database
mysql -uroot -pqpalwosk10 -e 'create database prestashop'

echo "Installing prestashop..."
sudo php "/vagrant/prestashop/install/index_cli.php" --domain=localhost:8081 --db_server=localhost --db_name=prestashop --db_user=root --db_password=qpalwosk10 --name xpectrum --email pub@prestashop.com --password qpalwosk10 --newsletter 0 --send_email 0

echo "Appliying security issues"
sudo rm -f -R "/vagrant/prestashop/install"
sudo mv /vagrant/prestashop/admin /vagrant/prestashop/xpec_admin

sudo sed -i 's/www-data/vagrant/g' /etc/apache2/envvars 

echo "Installation end, restarting apache2"
sudo service apache2 restart

echo "Xpectrum: Yesss !!!"

